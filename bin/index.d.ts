declare let primes: number[];
declare function getInitPrimes(): number[];
declare function getLastPrime(): number;
declare let log: (...str: any) => void;
/**
 * @name calculatePrimes
 *
 * @param until max number to calculate to | NOTICE: DOES NOT MEAN THAT lastPrime or the latest prime IS UNDER THAT NUMBER
 */
declare function calculatePrimes(until: number): {
    lastPrime: number;
    primes: number[];
};
declare function resetPrimes(): void;
//# sourceMappingURL=index.d.ts.map