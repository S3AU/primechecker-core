# PrimeChecker_Core

A library for generating a list of primes until (and including) a certain number.

## Installation
Use
```bash
yarn add primechecker_core
```

(or if you refuse to use yarn,
```bash
npm install primechecker_core --save
```
)

## Usage
*Some Usage Examples here will be in TypeScript. In JavaScript, these will be similar, but not identical*

### Loading
```ts
const primechecker = require("primechecker_core");
```
### Getting prime numbers until and including a number
If said number is 100, you would use...
```ts
const result = primechecker.calculatePrimes(100);
```
Which returns a table:
```json
{
    primes: [
        2,   3,  5,  7, 11, 13, 17, 19,
        23,  29, 31, 37, 41, 43, 47, 53,
        59,  61, 67, 71, 73, 79, 83, 89,
        97
    ]
    lastPrime: 97
}
```
***NOTICE:** Numbers previously generated will be listed here, regardless of the number you input. As an example, if you ran calculatePrimes(100) then calculatePrimes(10), the primes table would include the numbers 11-97 aswell.*

### Clearing the current data
Lets say for some reason, you didnt want the table anymore, and wanted to freshly generate it. You could use:
```ts
primechecker.resetPrimes();
```
Which returns nothing.

## Checking if a number is prime
Use `primechecker.isPrime`

Examples:
```ts
primechecker.isPrime(101);
// ↪ true

primechecker.isPrime(100);
// ↪ false
```
NOTICE: The above is more efficient than using
```ts
primechecker.calculatePrimes(101).primes.includes(101);
// ↪ 101 is used as an example
```
although the above also should work.

## NOTICES
- Does not work with negative numberrs
- 2 and 3 are primes by default

## Questions which might be on your mind
### Why does this have _core in the name?
Because I'm making an electron app based upon this.

### Why did you make this?
Someone made a scratch project and sent me it related to this topic, but it wasn't that complete, so i made this instead.
