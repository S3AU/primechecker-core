"use strict";

let timedOutInterval = 10^3; // change to the amount of loops allowed before timing out for a loop somewhere here


const getInitPrimes = () => {
    return [
        2,
        3
    ]
}

let primes = getInitPrimes()

const getLastPrime = () => {
    return primes[primes.length - 1]
}

let log = (...str:any)=>{}



const isPrime = (num:number) => { // efficient prime number checker
    let prime = num == 2 ? true : num != 1;
    for(let i=3; i<Math.sqrt(num); i=i+2) {
        if(num % i == 0) {
            prime = false;
            break;
        }
    }
    return prime;
}

/**
 * @name calculatePrimes
 * 
 * @param until max number to calculate to | NOTICE: DOES NOT MEAN THAT lastPrime or the latest prime IS UNDER THAT NUMBER
 */
const calculatePrimes = (until:number) => {
    let currentNumber=getLastPrime()
    function increment(){
        currentNumber=currentNumber+2
        log("Called Increment", currentNumber)
        let isPrime = true
        for (let index = 0; index < primes.length; index++) {
            log("For Loop", currentNumber)
            const element = primes[index];
            if (currentNumber%element==0){
                isPrime=false;
                break;
            }
        }
        if (isPrime){
            primes.push(currentNumber)
        }
        // primes.forEach(element => {
        //     if (currentNumber%element==0){
        //         isPrime=false
        //     }
        // });
        if (until <= currentNumber) {
            return {
                "output": {
                    "lastPrime": getLastPrime(),
                    "primes": primes
                },
                "sendOutput":true
            }
        } else {
            return {
                "sendOutput":false
            }
        }
    }
    let i = 0;
    while (true) {
        let o = increment();
        if (o.sendOutput==true){
            return o.output;
        }
        i=i+1;
        if (i == (timedOutInterval)) {
            throw new Error("Timed out");
        }
    }
}

function resetPrimes(){
    primes=getInitPrimes()
}

timedOutInterval = timedOutInterval > 0 ? timedOutInterval : 10^2

module.exports = {
    getLastPrime: getLastPrime,
    calculatePrimes: calculatePrimes,
    resetPrimes: resetPrimes,
    isPrime: isPrime
}
